# How to run DDBStreams Source Connector

The connector will read messages from `ew1-integration-abt-Tostokens` dynamodb streams and will publish them as `Token` fact in the local created topic `TokenDynamoStream`

## Prerequisites
You need to have installed:
- Docker and Docker compose
- [AWS CLI](https://aws.amazon.com/cli/)
- Credentials to access AWS, see [here](https://sites.google.com/masabi.com/platformitops/aws/aws-cli?pli=1#h.kpy4cmdzidsx) for more details. 

  In particular, when using docker-compose, it is assumed that you have the files `~/.aws/config` and `~/.aws/credentials/` configured as in the document.
- [jq](https://stedolan.github.io/jq/) to print in more human readable format what you get from the server responses.

## Run the connector in local with Docker-compose
- First, you need to build the `plugins` submodule and create an uber jar:
  ```bash
  ./gradlew shadowJar
  ```
- To run the connector and Kafka locally, using Docker Compose:
```bash
 docker-compose -f docker-compose.yml -f docker-compose-kafka-ui.yml -f docker-compose-ddbstreams-source-connector.yml up -d --build --remove-orphans
```

Once all containers are up and running:

- Verify the installation went ok, you should be able to see the connector `org.apache.camel.kafkaconnector.awsddbstreamssource.CamelAwsddbstreamssourceSourceConnector` from list in the following response:
  - `curl localhost:8083/connector-plugins | jq` 
- Create a local topic where `Token` messages would be published. 
  - [http://localhost:8080/ui/docker-kafka-server/topic/create](http://localhost:8080/ui/docker-kafka-server/topic/create) 
    - Name: `TokenDynamoStream`
- Connector is configured to read dynamodb streams from `ew1-integration-abt-Tostokens`

 
- Configure and launch the connector:
  ```shell
  curl -X POST -H  "Content-Type:application/json" --data "@./test-ddbstreams-remote.json" http://localhost:8083/connectors | jq
  ```
- Check if the connector now exists and runs correctly: 
  - To see if it's been configured [http://localhost:8083/connectors?expand=info](http://localhost:8083/connectors?expand=info)
  - To see its status [http://localhost:8083/connectors/ddbstreams-source-connector/status](http://localhost:8083/connectors/ddbstreams-source-connector/status)

- If you want to delete the connector and start again:
  ```shell
  curl -X DELETE http://localhost:8083/connectors/ddbstreams-source-connector
  ```

- Run some test:
  - Open [integration simulator](http://eu-west-1-integration.abt.private/tap/simulator/#/TESTABT/hub/createsmartcard) and create a token
  - Token facts should appear as topic message [http://localhost:8080/ui/docker-kafka-server/topic/TokenDynamoStream/data?sort=Oldest&partition=All](http://localhost:8080/ui/docker-kafka-server/topic/TokenDynamoStream/data?sort=Oldest&partition=All)

## Other useful info

### Connector configuration
- See all possible configurations for the Camel DDB Streams Source Connector:

```shell
curl -X PUT http://localhost:8083/connector-plugins/CamelAwsddbstreamssourceSourceConnector/config/validate \
     -H  "Content-Type:application/json" \
     -d '{
        "connector.class": "org.apache.camel.kafkaconnector.awsddbstreamssource.CamelAwsddbstreamssourceSourceConnector"
        }' | jq > source-connector-all-configs.json
```

There is a list of all the specific configuration saved in file [source-connector-all-configs](./source-connector-all-configs.json).

- You can read more about the configurations for the camel dynamodb streams source connector [here](https://camel.apache.org/camel-kafka-connector/next/reference/connectors/camel-aws-ddb-streams-source-kafka-source-connector.html)
- To know more about the Camel specific configuration go [here](https://camel.apache.org/camel-kafka-connector/3.18.x/user-guide/basic-configuration.html)
- For Kafka Connect specific configuration look at the [Apache Kafka documentation](https://kafka.apache.org/documentation/#connectconfigs)

## About plugin
`BytesRecordDataTransforms` is a [Kafka Transformation](https://www.confluent.io/blog/kafka-connect-single-message-transformation-tutorial-with-examples/) which read bytes stream from dynamodb record and transform it to the `json`.

To configure plugin to use `BytesRecordDataTransforms` is just to add it to the plugin known classpath and configure it the [plugin transforms value config](./test-ddbstreams-remote.json).

### To build a zip connector that can be uploaded to [MSK](https://gitlab.com/masabi/developer-tooling/msk-connectors#custom-plugin)

- Run `./gradlew buildShadowJar` 
- Copy `plugins/build/libs/plugins-all.jar` to `plugins/connector/camel-aws-ddb-streams-source-kafka-connector-bytes-to-json-3.18.2.zip#lib/`
> To unzip a connector use whatever tool you want. This is a temporary step until some kind of build job will be setup
 
>The original zip file is downloaded from [here](https://repo.maven.apache.org/maven2/org/apache/camel/kafkaconnector/camel-aws-ddb-streams-source-kafka-connector/3.18.2/camel-aws-ddb-streams-source-kafka-connector-3.18.2-package.tar.gz)